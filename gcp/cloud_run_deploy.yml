###############################################################################
## This is the ci/cd template to help automate a cloud run deployment
## STEPS:
## 1. Make sure to set environment variables for the GGL_SERVICE_ACCT (which is the service account JWT) see this https://gitlab.com/-/snippets/2381284 for permissions needed
## 2. Check out https://cs.gitlabdemo.cloud/jbowles-group/bowlz-landing/security-demo/my-templates/-/blob/main/example_cloudrun.gitlab-ci.yml on how to use this
## 3. This template utilizes google cloud build to build the dockerfile and push to google artifact repository (link to steps)
## 4. The image tags pushed are 2 (commit based and latest) which is setup per 
##    per environment
## 5. If you use environments and the different branches you could (based on this template) 
##    four different apps running all with slightly different app suffixes (this is how review apps work too)
###############################################################################
include:
  - template: Jobs/Code-Quality.gitlab-ci.yml  # https://gitlab.com/gitlab-org/gitlab/blob/master/lib/gitlab/ci/templates/Jobs/Code-Quality.gitlab-ci.yml
  - template: Jobs/Code-Intelligence.gitlab-ci.yml  # https://gitlab.com/gitlab-org/gitlab/blob/master/lib/gitlab/ci/templates/Jobs/Code-Intelligence.gitlab-ci.yml
  - template: Jobs/Browser-Performance-Testing.gitlab-ci.yml  # https://gitlab.com/gitlab-org/gitlab/blob/master/lib/gitlab/ci/templates/Jobs/Browser-Performance-Testing.gitlab-ci.yml
  - template: Security/DAST.gitlab-ci.yml  # https://gitlab.com/gitlab-org/gitlab/blob/master/lib/gitlab/ci/templates/Security/DAST.gitlab-ci.yml
  - template: Security/Container-Scanning.gitlab-ci.yml  # https://gitlab.com/gitlab-org/gitlab/blob/master/lib/gitlab/ci/templates/Security/Container-Scanning.gitlab-ci.yml
  - template: Security/Dependency-Scanning.gitlab-ci.yml  # https://gitlab.com/gitlab-org/gitlab/blob/master/lib/gitlab/ci/templates/Security/Dependency-Scanning.gitlab-ci.yml
  - template: Security/License-Scanning.gitlab-ci.yml  # https://gitlab.com/gitlab-org/gitlab/blob/master/lib/gitlab/ci/templates/Security/License-Scanning.gitlab-ci.yml
  - template: Security/SAST.gitlab-ci.yml  # https://gitlab.com/gitlab-org/gitlab/blob/master/lib/gitlab/ci/templates/Security/SAST.gitlab-ci.yml
  - template: Security/Secret-Detection.gitlab-ci.yml  # https://gitlab.com/gitlab-org/gitlab/blob/master/lib/gitlab/ci/templates/Security/Secret-Detection.gitlab-ci.yml
  - template: Security/API-Fuzzing.gitlab-ci.yml  # https://gitlab.com/gitlab-org/gitlab/blob/master/lib/gitlab/ci/templates/Security/API-Fuzzing.gitlab-ci.yml
  - local: gcp/gcr_build.yml

.common_rules: &common_rules
  - if: $CI_DEFAULT_BRANCH != $CI_COMMIT_REF_NAME
    when: never
  - if: $DAST_DISABLED || $DAST_DISABLED_FOR_DEFAULT_BRANCH
    when: never
  - if: $DAST_WEBSITE  # we don't need to create a review app if a URL is already given
    when: never

.ggl-deploy: &deploy_template
   image: google/cloud-sdk:latest
   extends: .ggl-base
   script:
    - source dockervars.env
    - echo "Deploying image --> ${BASE_IMAGE_TAG}"
    - echo "DOCKER_IMAGE=${BASE_IMAGE_TAG}" >> deploy.env
    - gcloud run deploy ${GGL_DEPLOY_APP} --image=$BASE_IMAGE_TAG --region=$GGL_REGION --allow-unauthenticated --project $GGL_PROJECT_ID $GGL_XTRA_ARGS
    - DEPLOYED_URL=`gcloud run services describe ${GGL_DEPLOY_APP} --platform managed --region ${GGL_REGION} --format 'value(status.url)' --project ${GGL_PROJECT_ID}`
    - echo "DEPLOYED_URL=${DEPLOYED_URL}" >> deploy.env
    - echo $DEPLOYED_URL > environment_url.txt
    - cp deploy.env gl_environment.env
   artifacts:
    name: "$CI_JOB_NAME"
    paths:
      - environment_url.txt
      - gl_environment.env
    when: always
    reports:
      dotenv: deploy.env
   environment:
    url: $DEPLOYED_URL

.ggl_cloudrun_stop:
  image: google/cloud-sdk:latest
  extends: .ggl-base
  stage: cleanup
  script:
    - gcloud run services delete ${GGL_DEPLOY_APP} --region $GGL_REGION --project $GGL_PROJECT_ID --quiet
  environment:
    action: stop
  allow_failure: true

echo_vars_deploy:
  stage: test
  script:
    - echo "CI_COMMIT_REF_NAME --> ${CI_COMMIT_REF_NAME}"
    - echo "GGL_PROJECT_ID --> ${GGL_PROJECT_ID}"
    - echo "GGL_REGION --> ${GGL_REGION}"
    - echo "GGL_DEPLOY_APP --> ${GGL_DEPLOY_APP}"

google_cloudrun_build:
  extends: .ggl_cloudrun_build
  stage: build

google_cloudrun_review:
  extends: .ggl-deploy
  stage: review
  environment:
    on_stop: google_cloudrun_review_stop
  rules:
    - if: '$CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH'
      when: never
    - if: '($CI_COMMIT_BRANCH == "qa" || $CI_COMMIT_BRANCH == $QA_BRANCH_NAME)'
      when: never
    - if: '($CI_COMMIT_BRANCH == "nonprod" || $CI_COMMIT_BRANCH == $NONPROD_BRANCH_NAME)'
      when: never
    - if: '($CI_COMMIT_BRANCH == "develop" || $CI_COMMIT_BRANCH == $DEV_BRANCH_NAME)'
      when: never
    - if: '$REVIEW_DISABLED'
      when: never
    - if: '$CI_COMMIT_TAG || $CI_COMMIT_BRANCH'

google_cloudrun_review_stop:
  extends: .ggl_cloudrun_stop
  rules:
    - if: '$CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH'
      when: never
    - if: '$REVIEW_DISABLED'
      when: never
    - if: '($CI_COMMIT_BRANCH == "qa" || $CI_COMMIT_BRANCH == $QA_BRANCH_NAME)'
      when: never
    - if: '($CI_COMMIT_BRANCH == "nonprod" || $CI_COMMIT_BRANCH == $NONPROD_BRANCH_NAME)'
      when: never
    - if: '($CI_COMMIT_BRANCH == "develop" || $CI_COMMIT_BRANCH == $DEV_BRANCH_NAME)'
      when: never
    - if: '$CI_COMMIT_TAG || $CI_COMMIT_BRANCH'
      when: manual

dast_env_cloudrun_deploy:
  extends: .ggl-deploy
  variables:
    GGL_DEPLOY_APP: "${APP_NAME}-dast"
    GIT_STRATEGY: none
  stage: review
  environment:
    name: dast-default
    on_stop: stop_dast_cloudrun_env
  rules:
    - *common_rules
    - if: $CI_COMMIT_BRANCH
      when: always

stop_dast_cloudrun_env:
  extends: .ggl_cloudrun_stop
  environment:
    name: dast-default
  rules:
    - *common_rules
    - if: $CI_COMMIT_BRANCH
      when: always

deployment:
  stage: deployment
  extends: .ggl-deploy
  environment:
    on_stop: deployment_stop
  rules:
    - if: '$CI_DEPLOY_FREEZE'
      when: never
    - if: '$STAGING_ENABLED'
      when: never
    - if: '$CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH'
    - if: '($CI_COMMIT_BRANCH == "qa" || $CI_COMMIT_BRANCH == $QA_BRANCH_NAME)'
    - if: '($CI_COMMIT_BRANCH == "nonprod" || $CI_COMMIT_BRANCH == $NONPROD_BRANCH_NAME)'
    - if: '($CI_COMMIT_BRANCH == "develop" || $CI_COMMIT_BRANCH == $DEV_BRANCH_NAME)'

deployment_manual:
  allow_failure: false
  stage: deployment
  extends: .ggl-deploy
  environment:
    on_stop: deployment_stop
  rules:
    - if: '$CI_DEPLOY_FREEZE'
      when: never
    - if: '$CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH && $STAGING_ENABLED'
      when: manual
    - if: '($CI_COMMIT_BRANCH == "develop" || $CI_COMMIT_BRANCH == $DEV_BRANCH_NAME) && $STAGING_ENABLED'
      when: manual
    - if: '($CI_COMMIT_BRANCH == "qa" || $CI_COMMIT_BRANCH == $QA_BRANCH_NAME) && $STAGING_ENABLED'
      when: manual
    - if: '($CI_COMMIT_BRANCH == "nonprod" || $CI_COMMIT_BRANCH == $NONPROD_BRANCH_NAME) && $STAGING_ENABLED'
      when: manual

deployment_stop:
  extends: .ggl_cloudrun_stop
  rules:
    - if: '$CI_DEPLOY_FREEZE'
      when: never
    - if: '$CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH'
      when: manual
    - if: '($CI_COMMIT_BRANCH == "qa" || $CI_COMMIT_BRANCH == $QA_BRANCH_NAME)'
      when: manual
    - if: '($CI_COMMIT_BRANCH == "nonprod" || $CI_COMMIT_BRANCH == $NONPROD_BRANCH_NAME)'
      when: manual
    - if: '($CI_COMMIT_BRANCH == "develop" || $CI_COMMIT_BRANCH == $DEV_BRANCH_NAME)'
      when: manual

