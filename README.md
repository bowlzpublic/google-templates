# My Templates

Repo for templates

## GOOGLE CLOUD RUN

Google cloud run is a serverless infrastructure created by google and mirrors AWS Fargate in many ways.

The templates located in [gcp](https://cs.gitlabdemo.cloud/jbowles-group/bowlz-landing/security-demo/my-templates/-/tree/main/gcp) are the basis of deploying an app to GOOGLE CLOUD RUN by just setting up a Dockerfile.  

The templates in the `gcp` folder are designed to mimic the behavior of [Auto DevOps](https://docs.gitlab.com/ee/topics/autodevops/), but using Cloud Run instead of Kubernetes.

The following are features of the template:
- Docker build
- Scanning
  - SAST
  - DAST
  - Container
  - Dependency
  - Code Quality
  - Secret Detection
  - API Fuzzing
- Review apps is also supported for merge request builds
- DAST also deploys a version of the app (re-using the review app config) for DAST scans

The following Google Services are used to make this happen
1. [Google Cloud Build](https://cloud.google.com/build/docs/build-push-docker-image) for building docker images
2. [Google Artifact Registry](https://cloud.google.com/artifact-registry/docs/docker/store-docker-container-images) for hosting the built images
3. [Google Service Accounts](https://cloud.google.com/iam/docs/service-accounts) to run this via Gitlab Runners
4. [Google Cloud Run](https://cloud.google.com/run/docs/quickstarts/deploy-container) by default this template utilizes [unauthenticated requests](https://cloud.google.com/run/docs/authenticating/public), TODO is to make this a toggle feature via CI/CD variables

### Google Setup
1. [Enable the Cloud Build API](https://console.cloud.google.com/flows/enableapi?apiid=cloudbuild)
2. [Enable Artifact Registry](https://console.cloud.google.com/flows/enableapi?apiid=artifactregistry.googleapis.com) & [Setup Docker Repo](https://cloud.google.com/artifact-registry/docs/docker/store-docker-container-images#create)
   - Take note of the name here, that will be the value of the variable `GGL_REGISTRY_FOLDER`
   - Take note of the location, for example `us-central1`, this will be used for variable `GGL_REGION` and will be part of the name for `GGL_REGISTRY_BASE` (i.e. `us-central1-docker.pkg.dev`)
3. Create your service account
   - This is probably the hardest part and you could take the easy way out and create a service account with "project owner" role, but then you would be over sharing access.
   - The bare minimum permissions needed are listed [here](https://gitlab.com/-/snippets/2381284)
   - best way to accomplish is to create a new role and assign that role to a service account (referenced in snippet above)
   - ![image.png](/uploads/f062f7f13e8a88382919254369ad68ca/image.png)
4. [Enable Cloud Run](https://console.cloud.google.com/apis/library/run.googleapis.com)
5. Upload your service account JWT to Gitlab CI/CD Variables
   1. Go to your new service account created in step 3.  [Google Service Accounts](https://console.cloud.google.com/iam-admin/serviceaccounts)
   2. Click on the Service Account email link
   3. Under the "KEYS" tab click "ADD KEY --> Create new key"
   4. Select "JSON" and then click "CREATE"
   5. This will download a `.json` file., open that file and copy the entire contents
   6. Now go to your GitLab project or group and select "Settings --> CI/CD --> Variables" and click "Add variable"
   7. Make the key name `GGL_SERVICE_ACCT` and paste your copied file contents into the "Value" section.
   8. Select "File" from the "Type" drop down and then uncheck "Protect variable". (untested if this works with it being protected??)
   9. Click "Add Variable"
   10. Done
### Using the Template
#### Include the cloud run template
To get started all you need to do is include the [cloud run template](https://cs.gitlabdemo.cloud/jbowles-group/bowlz-landing/security-demo/my-templates/-/blob/main/gcp/cloud_run_deploy.yml)

```
### --------------------------------- ###
### Including template for cloud run  ###
### --------------------------------- ###
include:
  - project: 'jbowles-group/bowlz-landing/security-demo/my-templates'
    ref: main
    file: '/gcp/cloud_run_deploy.yml'
```
#### Add in the variables
Update your `.gitlab-ci.yml` variables:
```
### --------------------------------- ###
### GOOGLE REQUIRED VARIABLES:        ###
###  -- APP_NAME                      ###  
###  -- GGL_REGISTRY_BASE             ###
###  -- GGL_REGION                    ###
###  -- GGL_SERVICE_ACCT_NAME         ###
###  -- GGL_PROJECT_ID                ###
###  -- GGL_SERVICE_ACCT (secret)     ###
###                                   ###
###  -------------------------------  ###
### GOOGLE OPTIONAL VARIABLES:        ###
###  --- GGL_XTRA_ARGS (used this to  ###
###      pass environment vars)       ###
### --------------------------------- ###
```
This can and probably should be done using Setting -> CI/CD -> Variables.  Note that if your deploying a lot of projects you should consider setting some of these at the group level:
- GGL_REGISTRY_BASE
- GGL_REGION
- GGL_PROJECT_ID
- GGL_SERVICE_ACCT

#### Override `container_scanning`
if you are using container scanning make sure to override the job step in the following way:
```
container_scanning:
  variables:
    SECURE_LOG_LEVEL: info
  before_script:
    - source dockervars.env
    - export DOCKER_PASSWORD=$(cat ${GGL_SERVICE_ACCT})
    - export DOCKER_USER="_json_key"
  dependencies:
    - google_cloudrun_build
```

This will make sure the build is run first and the environment variables are available

#### Example implementation
[Example](example_cloudrun.gitlab-ci.yml)

Full copy pasted here too:
```
### --------------------------------- ###
### Including template for cloud run  ###
### --------------------------------- ###
include:
  - project: 'jbowles-group/bowlz-landing/security-demo/my-templates'
    ref: main
    file: '/gcp/cloud_run_deploy.yml'

### --------------------------------- ###
### GOOGLE REQUIRED VARIABLES:        ###
###  -- APP_NAME                      ###  
###  -- GGL_REGISTRY_BASE             ###
###  -- GGL_REGION                    ###
###  -- GGL_SERVICE_ACCT_NAME         ###
###  -- GGL_PROJECT_ID                ###
###  -- GGL_SERVICE_ACCT (secret)     ###
###                                   ###
###  -------------------------------  ###
### GOOGLE OPTIONAL VARIABLES:        ###
###  --- GGL_XTRA_ARGS (used this to  ###
###      pass environment vars)       ###
### --------------------------------- ###
variables:
  APP_NAME: cloudrun-example-app
  GGL_REGISTRY_BASE: us-central1-docker.pkg.dev   #https://cs.gitlabdemo.cloud/jbowles-group/bowlz-landing/security-demo/my-templates/-/blob/main/gcp/common.yml
  GGL_REGION: us-central1.  #https://cs.gitlabdemo.cloud/jbowles-group/bowlz-landing/security-demo/my-templates/-/blob/main/gcp/common.yml
  GGL_REGISTRY_FOLDER: cloudrun-example-folder   #https://cs.gitlabdemo.cloud/jbowles-group/bowlz-landing/security-demo/my-templates/-/blob/main/gcp/common.yml
  GGL_SERVICE_ACCT_NAME: cloudrun-example-deployer@your-projectid.iam.gserviceaccount.com
  GGL_PROJECT_ID: your-projectid
  DEV_BRANCH_NAME: 1-my-first-issue  #Update to the branch you are using for development (could be an issue branch), assumes gitlab flow pattern only need if it is different than the default of 'develop'
  GGL_XTRA_ARGS: "--update-env-vars POSTGRES_HOST=${POSTGRES_HOST},APP_VAR=${APP_VAR}"  # Passing in environment vars to the cloud build job

### --------------------------------- ###
### Include all the stages and        ###
###   And disable them through vars   ###
### --------------------------------- ###
stages:
  - build
  - sync
  - test
  - deploy # dummy stage to follow some kind of guideline (copied from elsewhere)
  - review
  - fuzz
  - dast
  - staging
  - canary
  - deployment
  - performance
  - cleanup

container_scanning:
  variables:
    SECURE_LOG_LEVEL: info
  before_script:
    - source dockervars.env
    - export DOCKER_PASSWORD=$(cat ${GGL_SERVICE_ACCT})
    - export DOCKER_USER="_json_key"
  dependencies:
    - google_cloudrun_build
```
#### Debugging
There are jobs built in to echo out the value of variables used (except the service account ones), by default these will always run.  To turn off, set the CI/CD variable `GCP_DISABLE_ECHO_JOB` to `true`.
